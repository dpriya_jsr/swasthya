﻿namespace Common.ApiRequestModels.PatientRequestModels
{
    public class LoginPatientRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
