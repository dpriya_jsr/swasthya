﻿namespace Common.ApiRequestModels.HospitalRequestModels
{
    public class LoginHospitalRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
