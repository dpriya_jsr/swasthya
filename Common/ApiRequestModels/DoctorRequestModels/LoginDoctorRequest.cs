﻿namespace Common.ApiRequestModels.DoctorRequestModels
{
    public class LoginDoctorRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
